start:
	docker-compose up -d

stop:
	docker-compose stop

build:
	docker-compose rm -vsf
	docker-compose down -v --remove-orphans
	docker-compose build
	docker-compose up -d
